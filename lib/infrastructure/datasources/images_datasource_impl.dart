import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:wisy_challenge/domain/datasources/images_datasource.dart';
import 'package:wisy_challenge/domain/entities/images.dart';

class ImagesDatasourceImpl extends ImagesDatasource {
  final firebase = FirebaseFirestore.instance;

  @override
  Future<bool> createImage(Map<String, dynamic> imageLike) async {
    try {
      await firebase.collection('images').add(imageLike);

      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Future<List<Images>> getImages() async {
    QuerySnapshot<Map<String, dynamic>> snapshot =
        await FirebaseFirestore.instance.collection('images').get();

    List<Images> images = [];

    for (DocumentSnapshot<Map<String, dynamic>> doc in snapshot.docs) {
      Map<String, dynamic> data = doc.data()!;
      Images jsonToEntity = Images(
        id: data['id'],
        createDate: data['createDate'],
        path: data['path'],
      );
      images.add(jsonToEntity);
    }

    return images;
  }
}
