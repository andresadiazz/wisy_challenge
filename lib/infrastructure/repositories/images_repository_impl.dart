import 'package:wisy_challenge/domain/datasources/images_datasource.dart';
import 'package:wisy_challenge/domain/entities/images.dart';
import 'package:wisy_challenge/domain/repositories/images_repository.dart';

class ImagesRepositoryImpl extends ImagesRepository {
  final ImagesDatasource imagesDatasource;

  ImagesRepositoryImpl(this.imagesDatasource);

  @override
  Future<bool> createImage(Map<String, dynamic> imageLike) {
    return imagesDatasource.createImage(imageLike);
  }

  @override
  Future<List<Images>> getImages() {
    return imagesDatasource.getImages();
  }
}
