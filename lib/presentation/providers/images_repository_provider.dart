import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:wisy_challenge/domain/repositories/images_repository.dart';
import 'package:wisy_challenge/infrastructure/datasources/images_datasource_impl.dart';
import 'package:wisy_challenge/infrastructure/repositories/images_repository_impl.dart';

final imagesRepositoryProvider = Provider<ImagesRepository>((ref) {
  final imagesDatasource = ImagesDatasourceImpl();
  final imagesRepository = ImagesRepositoryImpl(imagesDatasource);

  return imagesRepository;
});
