import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:wisy_challenge/domain/entities/images.dart';
import 'package:wisy_challenge/domain/repositories/images_repository.dart';
import 'package:wisy_challenge/presentation/providers/images_repository_provider.dart';

final imagesProvider =
    StateNotifierProvider<ImagesNotifier, ImagesState>((ref) {
  final imagesRepository = ref.watch(imagesRepositoryProvider);

  return ImagesNotifier(imagesRepository: imagesRepository);
});

class ImagesNotifier extends StateNotifier<ImagesState> {
  final ImagesRepository imagesRepository;

  ImagesNotifier({required this.imagesRepository}) : super(ImagesState()) {
    getImages();
  }

  void _saveImageToFirebase(File? capturedImage) async {
    if (capturedImage != null) {
      // Guardar imagen en Firebase Storage
      final Reference storageReference =
          FirebaseStorage.instance.ref().child('images/${DateTime.now()}.png');
      await storageReference.putFile(capturedImage);

      // Guardar referencia a la imagen en Firestore
      final imageUrl = await storageReference.getDownloadURL();
      state = state.copyWith(pathImage: imageUrl);
    }
  }

  Future<bool> createImage(
    Map<String, dynamic> imagesLike,
  ) async {
    state = state.copyWith(isSaving: true);
    try {
      final save = await imagesRepository.createImage(imagesLike);
      state = state.copyWith(isSaving: false);
      return save;
    } catch (e) {
      state = state.copyWith(isSaving: false);
      return false;
    }
  }

  Future getImages() async {
    try {
      state = state.copyWith(isLoading: true);
      final images = await imagesRepository.getImages();

      if (images.isEmpty) {
        state = state.copyWith(isEmpty: true, isLoading: false);
      }

      state = state.copyWith(images: images, isLoading: false);
    } catch (e) {
      state = state.copyWith(isLoading: false);
    }
  }
}

class ImagesState {
  final bool isSaving;
  final bool isLoading;
  final bool isEmpty;
  final List<Images> images;
  final String? pathImage;

  ImagesState(
      {this.isLoading = false,
      this.isSaving = false,
      this.isEmpty = false,
      this.pathImage,
      this.images = const []});

  ImagesState copyWith(
          {bool? isLoading,
          bool? isEmpty,
          bool? isSaving,
          String? pathImage,
          List<Images>? images}) =>
      ImagesState(
          isSaving: isSaving ?? this.isSaving,
          isLoading: isLoading ?? this.isLoading,
          isEmpty: isEmpty ?? this.isEmpty,
          pathImage: pathImage ?? this.pathImage,
          images: images ?? this.images);
}
