import 'package:flutter/material.dart';
import 'package:wisy_challenge/config/router/app_router.dart';
import 'package:wisy_challenge/config/theme/app_theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: AppTheme().getTheme(),
      routerConfig: AppRouter.appRouter,
    );
  }
}
