import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:wisy_challenge/presentation/providers/images_provider.dart';

class ListImagesScreen extends ConsumerWidget {
  const ListImagesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final images = ref.watch(imagesProvider);
    return Scaffold(
        appBar: AppBar(),
        body: images.isLoading
            ? const Center(child: CircularProgressIndicator())
            : images.isEmpty
                ? const Center(
                    child: Text('First, you need to upload an image'),
                  )
                : Column(
                    children: [
                      ...images.images
                          .map((e) => Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(e.path))),
                              ))
                          .toList()
                    ],
                  ));
  }
}
