import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: SizedBox(
      width: size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () => context.push('/camera'),
              child: const Text('Upload Image')),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () => context.push('/listImages'),
              child: const Text('View Images List')),
        ],
      ),
    ));
  }
}
