import 'package:go_router/go_router.dart';
import 'package:wisy_challenge/presentation/screens/home/home.dart';
import 'package:wisy_challenge/presentation/screens/images/camera_awesome.dart';
import 'package:wisy_challenge/presentation/screens/images/list_images.dart';

class AppRouter {
  static final appRouter = GoRouter(initialLocation: '/home', routes: [
    GoRoute(
      path: '/home',
      builder: (context, state) => const HomeScreen(),
    ),
    GoRoute(
      path: '/camera',
      builder: (context, state) => const CameraAwesomeScreen(),
    ),
    GoRoute(
      path: '/listImages',
      builder: (context, state) => const ListImagesScreen(),
    ),
  ]);
}
