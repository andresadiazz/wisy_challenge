import '../entities/images.dart';

abstract class ImagesRepository {
  Future<bool> createImage(Map<String, dynamic> imageLike);
  Future<List<Images>> getImages();
}
