import 'dart:convert';

class Images {
  String id;
  DateTime createDate;
  String path;
  Images({
    required this.createDate,
    required this.id,
    required this.path,
  });

  factory Images.fromMap(Map<String, dynamic> map) {
    return Images(
      createDate: map['createDate'] ?? '',
      path: map['path'] ?? false,
      id: map['id'] ?? '',
    );
  }

  factory Images.fromJson(String source) => Images.fromMap(json.decode(source));
}
