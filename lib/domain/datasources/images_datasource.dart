import '../entities/images.dart';

abstract class ImagesDatasource {
  Future<bool> createImage(Map<String, dynamic> imageLike);
  Future<List<Images>> getImages();
}
